package br.com.mail.union;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnionMailApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnionMailApiApplication.class, args);
	}

}
