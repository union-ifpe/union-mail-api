package br.com.mail.union.service;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDateTime;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import br.com.mail.union.model.MailDTO;

@Service
public class MailService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

	private JavaMailSender mailSender;
	private SpringTemplateEngine springTemplateEngine;

	public MailService(JavaMailSender mailSender, SpringTemplateEngine springTemplateEngine) {
		this.mailSender = mailSender;
		this.springTemplateEngine = springTemplateEngine;
	}

	public void sendMail(MailDTO mailDTO) throws Exception {
		try {
			
			mailDTO.getProperties().put("currentYear", LocalDateTime.now().getYear());
			
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());
			Context context = new Context();

			context.setVariables(mailDTO.getProperties());
			helper.setFrom(mailDTO.getFrom());
			helper.setTo(mailDTO.getTo());
			helper.setSubject(mailDTO.getSubject());

			if (mailDTO.getHasAttachment()) {
				File file = ResourceUtils.getFile("classpath:csv-example.csv");
				if (file.exists()) {
					byte[] attachmentBytes = Files.readAllBytes(file.toPath());
					helper.addAttachment("csv-example.csv", new ByteArrayResource(attachmentBytes));
				}
			}

			String html = springTemplateEngine.process(mailDTO.getTemplate(), context);
			helper.setText(html, true);

			mailSender.send(message);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new Exception(e);
		}
	}
}
