package br.com.mail.union.exception;

public class SendMailException extends Exception {

	private static final long serialVersionUID = 1L;

	public SendMailException() {

	}

	public SendMailException(String message) {
		super(message);
	}

	public SendMailException(String message, Exception e) {
		super(message, e);
	}
}
